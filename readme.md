# How to use: 2019_07_29_MirrorGoBehaviour   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.mirrorgobehaviour":"https://gitlab.com/eloistree/2019_07_29_MirrorGoBehaviour.git",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.mirrorgobehaviour",                              
  "displayName": "Mirror Go Behaviour",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Simulate the Go controller behaviour when Touch is not detected",                         
  "keywords": ["Script","Tool"],                       
  "category": "Script",                   
  "dependencies":{}     
  }                                                                                
```    